tool
extends MeshInstance

class_name OrientedBezierCurve

export var draw_path = false
export var loop = false

var internal_draw_path = false
var bezier_points = []
var debug_draw_mesh: ArrayMesh
var divs = 10.0
var hair_length = 0.5
var line_material = null
var line_z_material = null
var line_x_material = null
var line_y_material = null

func _ready():
	_inits()	
	bezier_points = _create_bezier_points()
		
func _inits():	
	debug_draw_mesh = ArrayMesh.new()
	self.mesh = debug_draw_mesh
	line_material = SpatialMaterial.new()	
	line_material.flags_unshaded = true
	line_material.albedo_color = Color(1.0,1.0,1.0)
	#material_override = line_material
	line_z_material = SpatialMaterial.new()	
	line_z_material.flags_unshaded = true
	line_z_material.albedo_color = Color(0.2,0.4,1.0)
	
	line_x_material = SpatialMaterial.new()	
	line_x_material.flags_unshaded = true
	line_x_material.albedo_color = Color(1.0,0.2,0.4)
	
	line_y_material = SpatialMaterial.new()	
	line_y_material.flags_unshaded = true
	line_y_material.albedo_color = Color(0.2,1.0,0.4)

func _process(delta):
	if Engine.is_editor_hint():		
		if internal_draw_path or draw_path:
			_do_draw_path()
		else:
			if debug_draw_mesh != null:
				_clear_mesh()

func _clear_mesh():
	var sc = debug_draw_mesh.get_surface_count()
	for i in range(sc):
		debug_draw_mesh.surface_remove(0)	

func _create_bezier_points():
	var paths = get_children()
	var b_paths = []
	var path_count = paths.size()
	if path_count < 2: 
		return b_paths
	elif path_count == 2:
		b_paths.append(paths[0].transform)		
		b_paths.append(paths[0].transform.interpolate_with(paths[1].transform, 0.5))
		b_paths.append(paths[1].transform)
	elif path_count == 3 and not loop:
		b_paths.append(paths[0].transform)
		b_paths.append(paths[1].transform)
		b_paths.append(paths[2].transform)
	elif not loop:
		b_paths.append(paths[0].transform)
		b_paths.append(paths[1].transform)		
		for i in range(2, path_count-2, 1):			
			b_paths.append(paths[i-1].transform.interpolate_with(paths[i].transform, 0.5))
			b_paths.append(paths[i].transform)
		b_paths.append(paths[path_count-3].transform.interpolate_with(paths[path_count-2].transform, 0.5))		
		b_paths.append(paths[path_count-2].transform)
		b_paths.append(paths[path_count-1].transform)	
	else:		
		var middle = paths[0].transform.interpolate_with(paths[path_count-1].transform, 0.5)
		b_paths.append(middle)		
		for i in range(0, path_count-1):
			b_paths.append(paths[i].transform)
			b_paths.append(paths[i].transform.interpolate_with(paths[i+1].transform, 0.5))
		b_paths.append(paths[path_count-1].transform)
		b_paths.append(middle)		
	return b_paths

func _do_draw_path():
	if debug_draw_mesh == null:
		_inits()	
	_clear_mesh()	
	var pos_vertices = PoolVector3Array()	
	var dir_z_vertices = PoolVector3Array()	
	var dir_x_vertices = PoolVector3Array()	
	var dir_y_vertices = PoolVector3Array()	
	var b_paths = _create_bezier_points()

	var path_count = b_paths.size()-1
	divs = path_count*5
	var last_pos = null
	for i in range(divs+1.0):
		var pos = float(i)/divs
		var point = get_bezier_point(pos, b_paths)
		if last_pos !=null:
			hair_length += (last_pos-point.origin).length()
			hair_length /= 2
		last_pos = point.origin
		
		pos_vertices.push_back(point.origin)	
		dir_z_vertices.push_back(point.origin)
		dir_z_vertices.push_back(point.origin-point.basis.z*hair_length)
		dir_x_vertices.push_back(point.origin)
		dir_x_vertices.push_back(point.origin-point.basis.x*hair_length)
		dir_y_vertices.push_back(point.origin)
		dir_y_vertices.push_back(point.origin-point.basis.y*hair_length)
	var arrays = []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = pos_vertices
	debug_draw_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINE_STRIP, arrays)
	set_surface_material(0, line_material)	
	arrays[ArrayMesh.ARRAY_VERTEX] = dir_z_vertices
	debug_draw_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINES, arrays)
	set_surface_material(1, line_z_material)	
	arrays[ArrayMesh.ARRAY_VERTEX] = dir_x_vertices
	debug_draw_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINES, arrays)
	set_surface_material(2, line_x_material)	
	arrays[ArrayMesh.ARRAY_VERTEX] = dir_y_vertices
	debug_draw_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINES, arrays)
	set_surface_material(3, line_y_material)	

func get_bezier_point(x, b_points=null):
	if b_points == null:
		b_points = bezier_points
	var path_count = b_points.size()-1.0	
	var pos = path_count*x
	var current = round(pos)
	if fmod(current,2.0)==0:
		if current<pos:
			current+=1
		else:
			current-=1	
	current = clamp(current, 0, path_count)
	var prev = max(0.0, current-1.0)
	var next = min(path_count, current+1.0)
	var p0 = b_points[prev].origin
	var p1 = b_points[current].origin
	var p2 = b_points[next].origin
	var b0 = b_points[prev].basis
	var b1 = b_points[current].basis
	var b2 = b_points[next].basis
	var t = (pos-prev)/2.0
	var r01 = pow(1.0-t, 2.0)
	var r21 = pow(t, 2.0)
	var point = p1 + r01*(p0-p1) + r21*(p2-p1)
	var b1b2 = b1.slerp(b2, r21)
	var b1b0 = b1.slerp(b0, r01)
	var orientation = b1.slerp(b2, r21).slerp(b0, r01) 
	#if not Engine.is_editor_hint():		
	#print("x: " + str(x) + "t: " + str(t))
	return Transform(orientation, point) 


