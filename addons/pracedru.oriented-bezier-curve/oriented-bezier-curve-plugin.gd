tool
extends EditorPlugin
var eds = get_editor_interface().get_selection()
var drawn_curve_ref = null

func _enter_tree():
	add_custom_type(
		"OrientedBezierCurve", "MeshInstance",
		preload("oriented-bezier-curve.gd"),
		preload("oriented_bezier_curve_icon.png")
	)
	eds.connect("selection_changed", self, "_on_selection_changed")

func _exit_tree():
	remove_custom_type("OrientedBezierCurve")

func _on_selection_changed():
	var selected = eds.get_selected_nodes() 
	if not selected.empty():
		var selected_node = selected[0]
		if selected_node is OrientedBezierCurve:
			_draw_curve(selected_node)
		elif selected_node.get_parent() is OrientedBezierCurve:
			_draw_curve(selected_node.get_parent())
		else:
			if drawn_curve_ref != null:
				_clear_curve()
	else:
		if drawn_curve_ref != null:
			_clear_curve()

func _draw_curve(curve):
	if drawn_curve_ref != null:
		_clear_curve()
	curve.internal_draw_path = true
	drawn_curve_ref = weakref(curve)

func _clear_curve():
	var drawn_curve = drawn_curve_ref.get_ref()
	if drawn_curve != null:
		drawn_curve.internal_draw_path = false
		drawn_curve_ref = null
