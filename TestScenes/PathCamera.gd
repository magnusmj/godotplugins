extends Spatial

enum Modes {
	PingPong
	Loop
}

export var curve_path: NodePath
export (Modes)var mode  = Modes.Loop

var ratio = 0.0
var direction = 1.0

onready var curve: OrientedBezierCurve = get_node(curve_path)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	ratio += direction*delta/10
	if ratio <= 0 or ratio >= 1.0:
		if mode == Modes.PingPong:
			direction *= -1
			ratio = round(ratio)
		else:
			ratio = ratio - round(ratio)
	
	transform = curve.transform*curve.get_bezier_point(ratio)
	
